# README #

**Tinkoff News Sample without any libraries for API, database management and Model-View-Presenter**

Based on Kotlin and Android Studio 3.0

Features:

- API for news

- Cached news list in sqlite (SqliteOpenHelper)

- Update news by swipe to refresh 

- View selected article

- Model-View-Presenter without any libraries

- Packages by feature

- Sort news by pub date
