package tinkoff.ru.tinkoffnews.data.api

import android.os.AsyncTask
import tinkoff.ru.tinkoffnews.base.utils.IOUtils
import java.io.IOException
import java.io.InputStream
import java.net.URL
import javax.net.ssl.HttpsURLConnection


/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
object NewsApi {

    private val NEWS_URL = "https://api.tinkoff.ru/v1/news"
    private val ARTICLE_URL = "https://api.tinkoff.ru/v1/news_content?id="

    private val METHOD_GET = "GET"

    private val EMPTY_RESPONSE = ""

    private val TIMEOUT_READ = 3000
    private val TIMEOUT_CONNECT = 5000

    fun getNews(callback: RequestCallback): ApiRequestTask {
        val task = ApiRequestTask(NEWS_URL, callback)
        task.execute()
        return task
    }

    fun getArticleById(id:Int, callback: RequestCallback): ApiRequestTask {
        val task = ApiRequestTask(ARTICLE_URL+id, callback)
        task.execute()
        return task
    }

    class ApiRequestTask(val url: String,
                         val callback: RequestCallback):
            AsyncTask<String, Int, NewsApi.Result>() {


        override fun doInBackground(vararg params: String?): Result {
            var result = Result(EMPTY_RESPONSE)
            if (!isCancelled) {
                try {
                    val url = URL(url)
                    val resultString = downloadUrl(url)
                    if (resultString != null) {
                        result = Result(resultString)
                    } else {
                        throw IOException("No response received.")
                    }
                } catch (e: Exception) {
                    result = Result(error = e)
                }

            }
            return result
        }

        override fun onPostExecute(result: Result?) {
            if (result != null && callback != null) {
                if (result.error != null) {
                    callback.error(result.error)
                } else if (result.response != null) {
                    callback.successRequest(result.response)
                }
            }
        }

        @Throws(IOException::class)
        private fun downloadUrl(url: URL): String? {
            var stream: InputStream? = null
            var connection: HttpsURLConnection? = null
            var result: String? = null
            try {
                connection = url.openConnection() as HttpsURLConnection
                connection.readTimeout = TIMEOUT_READ
                connection.connectTimeout = TIMEOUT_CONNECT
                connection.requestMethod = METHOD_GET
                connection.doInput = true
                connection.connect()

                val responseCode = connection.responseCode
                if (responseCode != HttpsURLConnection.HTTP_OK) {
                    throw IOException("HTTP error code: " + responseCode)
                }

                stream = connection.inputStream
                if (stream != null) {
                    result = IOUtils.readInputStream(stream)
                }
            } finally {
                if (stream != null) {
                    stream!!.close()
                }

                if (connection != null) {
                    connection.disconnect()
                }
            }
            return result
        }
    }

    class Result(val response: String = EMPTY_RESPONSE,
                 val error: Throwable? = null)

    interface RequestCallback {
        fun error(error: Throwable)
        fun successRequest(response: String)
    }
}