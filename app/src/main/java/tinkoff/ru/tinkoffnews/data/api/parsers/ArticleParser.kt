package tinkoff.ru.tinkoffnews.data.api.parsers

import org.json.JSONObject
import tinkoff.ru.tinkoffnews.article.mvp.model.Article

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
object ArticleParser {

    private val ITEM_PAYLOAD = "payload"
    private val ITEM_CONTENT = "content"

    fun parseArticle(response: String): Article {
        val rootObj = JSONObject(response)
        var content = ""
        if (rootObj.has(ITEM_PAYLOAD)) {
            val payload = rootObj.getJSONObject(ITEM_PAYLOAD)
            if( payload.has(ITEM_CONTENT) ){
                content = payload.getString(ITEM_CONTENT)
            }
        }

        return Article(content)
    }
}