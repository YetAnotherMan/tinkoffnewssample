package tinkoff.ru.tinkoffnews.data.api.parsers

import org.json.JSONObject
import tinkoff.ru.tinkoffnews.news.mvp.model.News

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
object NewsParser {

    private val ITEM_PAYLOAD = "payload"
    private val ITEM_ID = "id"
    private val ITEM_TEXT = "text"
    private val ITEM_PUB_DATE = "publicationDate"
    private val ITEM_MILLIS = "milliseconds"

    fun parseNews(response: String): List<News> {
        val newsList = ArrayList<News>()

        val rootObj = JSONObject(response)

        if( rootObj.has(ITEM_PAYLOAD) ){
            val payloads = rootObj.getJSONArray(ITEM_PAYLOAD)
            for (i in 0..(payloads.length() - 1)) {
                val item = payloads.getJSONObject(i)

                var id = 0
                var text = ""
                var millis = 0L

                if( item.has(ITEM_ID) ) {
                    id = item.getInt(ITEM_ID)
                }

                if( item.has(ITEM_TEXT) ) {
                    text = item.getString(ITEM_TEXT)
                }

                if( item.has(ITEM_PUB_DATE) ) {
                    val pubDateItem = item.getJSONObject(ITEM_PUB_DATE)
                    if( pubDateItem.has(ITEM_MILLIS) ){
                        millis = pubDateItem.getLong(ITEM_MILLIS)
                    }
                }

                newsList.add(News(id,text, millis))
            }
        }

        return newsList
    }
}