package tinkoff.ru.tinkoffnews.data.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
class NewsSqliteOpenHelper(context: Context): SQLiteOpenHelper(context, "tinkoff_news.db", null, 1) {

    companion object {
        val TABLE_NEWS = "news"

        val COLUMN_ID = "_id"
        val COLUMN_TITLE = "title"
        val COLUMN_PUB_DATE = "pub_date"
    }

    private val DATABASE_NAME = "tinkoff_news.db"
    private val DATABASE_VERSION = 1

    // Database creation sql statement
    private val DATABASE_CREATE = ("create table "
            + TABLE_NEWS + "( " + COLUMN_ID
            + " integer primary key autoincrement, " + COLUMN_TITLE
            + " text not null, " + COLUMN_PUB_DATE + " integer not null" +
            ");")

    override fun onCreate(db: SQLiteDatabase?) {
        db?.execSQL(DATABASE_CREATE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        Log.w(NewsSqliteOpenHelper::class.java!!.getName(),
                "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data")
        db?.execSQL("DROP TABLE IF EXISTS " + TABLE_NEWS)
        onCreate(db)
    }
}