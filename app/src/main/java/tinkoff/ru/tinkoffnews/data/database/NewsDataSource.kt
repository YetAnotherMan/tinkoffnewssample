package tinkoff.ru.tinkoffnews.data.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import tinkoff.ru.tinkoffnews.news.mvp.model.News


/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
class NewsDataSource(context: Context) {

    private var database: SQLiteDatabase? = null
    private val dbHelper: NewsSqliteOpenHelper = NewsSqliteOpenHelper(context)

    private val INDEX_ID_COLUMN = 0
    private val INDEX_TITLE_COLUMN = 1
    private val INDEX_PUB_DATE_COLUMN = 2

    private val allColumns = arrayOf(
            NewsSqliteOpenHelper.COLUMN_ID,
            NewsSqliteOpenHelper.COLUMN_TITLE,
            NewsSqliteOpenHelper.COLUMN_PUB_DATE)

    @Throws(SQLException::class)
    fun open() {
        database = dbHelper.getWritableDatabase()
    }

    fun close() {
        dbHelper.close()
    }

    fun insertNews(newsList: List<News>) {
        database?.beginTransaction()
        newsList.forEach {
            val values = ContentValues()
            values.put(NewsSqliteOpenHelper.COLUMN_ID, it.id)
            values.put(NewsSqliteOpenHelper.COLUMN_PUB_DATE, it.pubDate)
            values.put(NewsSqliteOpenHelper.COLUMN_TITLE, it.title)

            database?.insert(NewsSqliteOpenHelper.TABLE_NEWS, null, values)
        }
        database?.setTransactionSuccessful()
        database?.endTransaction()
    }

    fun deleteNews() {
        database?.delete(NewsSqliteOpenHelper.TABLE_NEWS, null, null)
    }

    fun getAllNews(): List<News> {
        val comments = ArrayList<News>()

        val cursor = database!!.query(NewsSqliteOpenHelper.TABLE_NEWS,
                allColumns,
                null,
                null,
                null,
                null,
                null)

        cursor.moveToFirst()
        while (!cursor.isAfterLast) {
            val comment = cursorToNews(cursor)
            comments.add(comment)
            cursor.moveToNext()
        }
        cursor.close()
        return comments
    }

    private fun cursorToNews(cursor: Cursor): News {
        return News(cursor.getInt(INDEX_ID_COLUMN),
                cursor.getString(INDEX_TITLE_COLUMN),
                cursor.getLong(INDEX_PUB_DATE_COLUMN))
    }
}