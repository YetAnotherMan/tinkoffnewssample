package tinkoff.ru.tinkoffnews.base.utils

import java.io.InputStream
import java.util.*


/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
object IOUtils {

    fun readInputStream(stream: InputStream): String {
        val s = Scanner(stream).useDelimiter("\\A")
        return if (s.hasNext()) s.next() else ""
    }
}