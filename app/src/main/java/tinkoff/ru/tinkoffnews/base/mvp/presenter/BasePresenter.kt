package tinkoff.ru.tinkoffnews.base.mvp.presenter

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
interface BasePresenter {
    fun onResume()
    fun onDestroy()
}