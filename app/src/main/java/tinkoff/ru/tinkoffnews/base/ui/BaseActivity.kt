package tinkoff.ru.tinkoffnews.base.ui

import android.support.v7.app.AppCompatActivity
import tinkoff.ru.tinkoffnews.base.mvp.view.BaseView

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
open class BaseActivity : AppCompatActivity(), BaseView {

    override fun showError(message: String?) {
        //empty stuff
    }

    override fun showProgress() {
        //empty stuff
    }

    override fun hideProgress() {
        //empty stuff
    }
}