package tinkoff.ru.tinkoffnews.base.mvp.view

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
interface BaseView {

    fun showProgress()
    fun hideProgress()
    fun showError(message: String?)

}