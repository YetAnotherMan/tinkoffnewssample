package tinkoff.ru.tinkoffnews.article.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Html
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_article.*
import tinkoff.ru.tinkoffnews.R
import tinkoff.ru.tinkoffnews.article.mvp.model.Article
import tinkoff.ru.tinkoffnews.article.mvp.presenter.ArticlePresenter
import tinkoff.ru.tinkoffnews.article.mvp.presenter.ArticlePresenterImpl
import tinkoff.ru.tinkoffnews.article.mvp.view.ArticleView
import tinkoff.ru.tinkoffnews.base.ui.BaseActivity

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
class ArticleActivity: BaseActivity(), ArticleView {

    lateinit var presenter: ArticlePresenter

    companion object {
        val EXTRA_NEWS_ID = "extra_news_id"

        fun getIntent(context: Context, newsId: Int) =
                Intent(context, ArticleActivity::class.java).putExtra(EXTRA_NEWS_ID, newsId)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article)
        setTitle(R.string.title_article)

        presenter = ArticlePresenterImpl(this)
        init()
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun showArticle(content: Article) {
        tvArticle.text = Html.fromHtml(content.content)
    }

    override fun showError(message: String?){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun init() {
        val newsId = intent.extras.getInt(EXTRA_NEWS_ID)
        presenter.showArticle(newsId)
    }
}