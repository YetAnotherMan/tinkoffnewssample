package tinkoff.ru.tinkoffnews.article.mvp.presenter

import android.util.Log
import tinkoff.ru.tinkoffnews.article.mvp.view.ArticleView
import tinkoff.ru.tinkoffnews.data.api.NewsApi
import tinkoff.ru.tinkoffnews.data.api.parsers.ArticleParser

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
class ArticlePresenterImpl(var view: ArticleView?): ArticlePresenter {

    lateinit var articleTask: NewsApi.ApiRequestTask

    override fun onResume() {

    }

    override fun onDestroy() {
        view = null
        articleTask.cancel(true)
    }

    override fun showArticle(newsId: Int) {
        articleTask = NewsApi.getArticleById(newsId, object: NewsApi.RequestCallback {
            override fun successRequest(response: String) {
                Log.d(ArticlePresenterImpl::class.java.simpleName, "response:"+response)

                view?.hideProgress()
                view?.showArticle(ArticleParser.parseArticle(response))
            }

            override fun error(error: Throwable) {
                error.let {
                    Log.d(ArticlePresenterImpl::class.java.simpleName, "error:"+error.message)
                    view?.showError(error.message)
                }

                view?.hideProgress()
            }
        })
    }
}