package tinkoff.ru.tinkoffnews.article.mvp.view

import tinkoff.ru.tinkoffnews.article.mvp.model.Article
import tinkoff.ru.tinkoffnews.base.mvp.view.BaseView

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
interface ArticleView: BaseView {

    fun showArticle(content: Article)

}