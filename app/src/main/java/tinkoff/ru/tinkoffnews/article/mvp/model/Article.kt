package tinkoff.ru.tinkoffnews.article.mvp.model

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
data class Article(val content: String)