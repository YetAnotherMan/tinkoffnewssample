package tinkoff.ru.tinkoffnews.article.mvp.presenter

import tinkoff.ru.tinkoffnews.base.mvp.presenter.BasePresenter

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
interface ArticlePresenter: BasePresenter {
    fun showArticle(newsId: Int)
}