package tinkoff.ru.tinkoffnews.news.ui

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import tinkoff.ru.tinkoffnews.R
import tinkoff.ru.tinkoffnews.base.ui.BaseActivity
import tinkoff.ru.tinkoffnews.news.mvp.model.News
import tinkoff.ru.tinkoffnews.news.mvp.presenter.NewsPresenter
import tinkoff.ru.tinkoffnews.news.mvp.presenter.NewsPresenterImpl
import tinkoff.ru.tinkoffnews.news.mvp.view.NewsView
import tinkoff.ru.tinkoffnews.news.ui.adapter.NewsAdapter

class NewsActivity : BaseActivity(), NewsView {

    lateinit var presenter: NewsPresenter
    lateinit var adapter: NewsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setTitle(R.string.title_news)
        initViews()

        presenter = NewsPresenterImpl(this, this)
    }

    override fun onResume() {
        super.onResume()
        presenter.onResume()
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun hideProgress() {
        super.hideProgress()
        clpProgress.hide()
    }

    override fun showProgress() {
        super.showProgress()
        clpProgress.show()
    }

    override fun fillNewsList(news: List<News>) {
        adapter.addNews(news)
    }

    override fun stopRefreshing() {
        srlContent.isRefreshing = false
    }

    override fun showError(message: String?){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    fun initViews(){
        val divider = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        divider.setDrawable(ContextCompat.getDrawable(this, R.drawable.shape_divider))
        rvNews.addItemDecoration(divider)

        val manager = LinearLayoutManager(this)
        manager.orientation = LinearLayoutManager.VERTICAL
        rvNews.layoutManager = manager

        adapter = NewsAdapter()
        rvNews.adapter = adapter

        srlContent.setOnRefreshListener {
            adapter.deleteNews()
            presenter.pullToRefresh()
            srlContent.isRefreshing = true
        }
    }
}