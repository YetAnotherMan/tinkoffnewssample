package tinkoff.ru.tinkoffnews.news.mvp.model

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
data class News(val id: Int,
                val title: String,
                val pubDate: Long)