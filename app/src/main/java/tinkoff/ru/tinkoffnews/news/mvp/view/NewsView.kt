package tinkoff.ru.tinkoffnews.news.mvp.view

import tinkoff.ru.tinkoffnews.base.mvp.view.BaseView
import tinkoff.ru.tinkoffnews.news.mvp.model.News

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
interface NewsView: BaseView {

    fun fillNewsList(news: List<News>)
    fun stopRefreshing()

}