package tinkoff.ru.tinkoffnews.news.mvp.presenter

import tinkoff.ru.tinkoffnews.base.mvp.presenter.BasePresenter

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
interface NewsPresenter: BasePresenter {
    fun pullToRefresh()
}