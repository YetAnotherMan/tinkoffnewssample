package tinkoff.ru.tinkoffnews.news.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import tinkoff.ru.tinkoffnews.R
import tinkoff.ru.tinkoffnews.article.ui.ArticleActivity
import tinkoff.ru.tinkoffnews.news.mvp.model.News
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
class NewsAdapter: RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    private val newsList: ArrayList<News> = ArrayList()

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        val news = newsList[position]
        holder?.bind(news)
    }

    override fun getItemCount() = newsList.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent?.context).inflate(R.layout.item_news, null)
        return ViewHolder(view)
    }

    fun addNews(news: List<News>){
        newsList.addAll(news)
        notifyDataSetChanged()
    }

    fun deleteNews(){
        newsList.clear()
        notifyDataSetChanged()
    }

    class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        fun bind(news: News){
            val tvTitle = view.findViewById<View>(R.id.tvTitle) as TextView
            tvTitle.text = news.title

            val tvPubDate = view.findViewById<View>(R.id.tvDate) as TextView

            val format = SimpleDateFormat("yyyy dd MMMM hh:mm")
            tvPubDate.text = format.format(Date(news.pubDate))

            view.setOnClickListener {
                view.context.startActivity(ArticleActivity.getIntent(view.context, news.id))
            }
        }
    }
}