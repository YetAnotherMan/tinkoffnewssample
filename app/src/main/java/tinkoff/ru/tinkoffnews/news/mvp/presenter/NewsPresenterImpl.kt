package tinkoff.ru.tinkoffnews.news.mvp.presenter

import android.content.Context
import android.util.Log
import tinkoff.ru.tinkoffnews.data.api.NewsApi
import tinkoff.ru.tinkoffnews.data.api.NewsApi.getNews
import tinkoff.ru.tinkoffnews.data.api.parsers.NewsParser
import tinkoff.ru.tinkoffnews.data.database.NewsDataSource
import tinkoff.ru.tinkoffnews.news.mvp.model.News
import tinkoff.ru.tinkoffnews.news.mvp.view.NewsView
import java.util.*

/**
 * Created by Aleksandr Pokhilko on 14.11.2017
 */
class NewsPresenterImpl(var view: NewsView?,
                        context: Context): NewsPresenter {

    var newsTask: NewsApi.ApiRequestTask? = null
    val dataSource = NewsDataSource(context)

    override fun onResume() {
        dataSource.open()

        val newsList = dataSource.getAllNews()
        if( newsList.isEmpty() ) {
            view?.showProgress()
            requestNews(false)
        } else {
            Collections.sort(newsList, NewsComparator())
            view?.hideProgress()
            view?.fillNewsList(newsList)
        }
    }

    override fun onDestroy() {
        view = null
        dataSource.close()
        newsTask?.cancel(true)
    }

    override fun pullToRefresh(){
        dataSource.deleteNews()
        requestNews(true)
    }

    private fun requestNews(withoutProgress: Boolean){
        newsTask = getNews(object: NewsApi.RequestCallback {
            override fun successRequest(response: String) {
                Log.d(NewsPresenterImpl::class.java.simpleName, "response:"+response)

                if( !withoutProgress )
                    view?.hideProgress()

                val newsList = NewsParser.parseNews(response)
                Collections.sort(newsList, NewsComparator())
                view?.fillNewsList(newsList)
                dataSource.insertNews(newsList)

                view?.stopRefreshing()
            }

            override fun error(error: Throwable) {
                error.let {
                    Log.d(NewsPresenterImpl::class.java.simpleName, "error:"+error.message)
                    view?.showError(error.message)
                }

                if( !withoutProgress )
                    view?.hideProgress()
                view?.stopRefreshing()
            }
        })
    }

    private class NewsComparator: Comparator<News> {
        override fun compare(first: News?, second: News?): Int {
            return second!!.pubDate.compareTo(first!!.pubDate)
        }
    }
}